<?php

namespace App\Tests\WampServer;

use App\WampServer\Realm\Web;
use App\WampServer\UriResolver;
use PHPUnit\Framework\TestCase;

class UriResolverTest extends TestCase
{
    private UriResolver $uriResolver;

    public function setUp(): void
    {
        parent::setUp();

        $this->uriResolver = new UriResolver();
    }

    /**
     * @dataProvider getTopicData
     */
    public function testGetTopic(string $expected, string $topic, array $parameters): void
    {
        $this->assertEquals($expected, $this->uriResolver->resolve($topic, $parameters));
    }

    public function getTopicData(): array
    {
        return [
            [
                Web::TOPIC_GROUP_CREATED,
                Web::TOPIC_GROUP_CREATED,
                [],
            ],
            [
                Web::TOPIC_GROUP_CREATED,
                Web::TOPIC_GROUP_CREATED,
                [
                    'undefinedParameter' => 'undefinedValue',
                ],
            ],
        ];
    }
}
