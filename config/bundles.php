<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Odevia\MicroserviceClientBundle\OdeviaMicroserviceClientBundle::class => ['all' => true],
    Odeven\AmqpBundle\OdevenAmqpBundle::class => ['all' => true],
];
