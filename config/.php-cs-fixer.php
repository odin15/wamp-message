<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__.'/..')
    ->exclude('var')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
    ])
    ->setFinder($finder)
    ->setCacheFile('var/.php_cs.cache')
;