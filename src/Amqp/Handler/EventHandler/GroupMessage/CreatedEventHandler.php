<?php

namespace App\Amqp\Handler\EventHandler\GroupMessage;

use App\Amqp\Message\Event\GroupMessage\CreatedEvent;
use App\WampServer\Publisher\GroupMessagePublisher;
use Symfony\Contracts\Service\Attribute\Required;

class CreatedEventHandler
{
    private GroupMessagePublisher $groupMessagePublisher;

    #[Required]
    public function setGroupMessagePublisher(GroupMessagePublisher $groupMessagePublisher): self
    {
        $this->groupMessagePublisher = $groupMessagePublisher;
        return $this;
    }

    public function __invoke(CreatedEvent $event)
    {
        $this->groupMessagePublisher->publishGroupMessageCreatedEvent($event);
    }
}
