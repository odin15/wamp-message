<?php

namespace App\Amqp\Handler\EventHandler\GroupMessage;

use App\Amqp\Message\Event\GroupMessage\UpdatedEvent;
use App\WampServer\Publisher\GroupMessagePublisher;
use Symfony\Contracts\Service\Attribute\Required;

class UpdatedEventHandler
{
    private GroupMessagePublisher $groupMessagePublisher;

    #[Required]
    public function setGroupMessagePublisher(GroupMessagePublisher $groupMessagePublisher): self
    {
        $this->groupMessagePublisher = $groupMessagePublisher;
        return $this;
    }

    public function __invoke(UpdatedEvent $event)
    {
        $this->groupMessagePublisher->publishGroupMessageUpdatedEvent($event);
    }
}
