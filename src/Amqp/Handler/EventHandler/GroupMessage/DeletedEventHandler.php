<?php

namespace App\Amqp\Handler\EventHandler\GroupMessage;

use App\Amqp\Message\Event\GroupMessage\DeletedEvent;
use App\WampServer\Publisher\GroupMessagePublisher;
use Symfony\Contracts\Service\Attribute\Required;

class DeletedEventHandler
{
    private GroupMessagePublisher $groupMessagePublisher;

    #[Required]
    public function setGroupMessagePublisher(GroupMessagePublisher $groupMessagePublisher): self
    {
        $this->groupMessagePublisher = $groupMessagePublisher;
        return $this;
    }

    public function __invoke(DeletedEvent $event)
    {
        $this->groupMessagePublisher->publishGroupMessageDeletedEvent($event);
    }
}
