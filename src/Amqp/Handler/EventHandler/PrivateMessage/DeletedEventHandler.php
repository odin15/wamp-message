<?php

namespace App\Amqp\Handler\EventHandler\PrivateMessage;

use App\Amqp\Message\Event\PrivateMessage\DeletedEvent;
use App\WampServer\Publisher\PrivateMessagePublisher;
use Symfony\Contracts\Service\Attribute\Required;

class DeletedEventHandler
{
    private PrivateMessagePublisher $privateMessagePublisher;

    #[Required]
    public function setPrivateMessagePublisher(PrivateMessagePublisher $privateMessagePublisher): self
    {
        $this->privateMessagePublisher = $privateMessagePublisher;
        return $this;
    }

    public function __invoke(DeletedEvent $event)
    {
        $this->privateMessagePublisher->publishPrivateMessageDeletedEvent($event);
    }
}
