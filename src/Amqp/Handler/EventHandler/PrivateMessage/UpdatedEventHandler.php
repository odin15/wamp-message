<?php

namespace App\Amqp\Handler\EventHandler\PrivateMessage;

use App\Amqp\Message\Event\PrivateMessage\UpdatedEvent;
use App\WampServer\Publisher\PrivateMessagePublisher;
use Symfony\Contracts\Service\Attribute\Required;

class UpdatedEventHandler
{
    private PrivateMessagePublisher $privateMessagePublisher;

    #[Required]
    public function setPrivateMessagePublisher(PrivateMessagePublisher $privateMessagePublisher): self
    {
        $this->privateMessagePublisher = $privateMessagePublisher;
        return $this;
    }

    public function __invoke(UpdatedEvent $event)
    {
        $this->privateMessagePublisher->publishPrivateMessageUpdatedEvent($event);
    }
}
