<?php

namespace App\Amqp\Handler\EventHandler\PrivateMessage;

use App\Amqp\Message\Event\PrivateMessage\CreatedEvent;
use App\WampServer\Publisher\PrivateMessagePublisher;
use Symfony\Contracts\Service\Attribute\Required;

class CreatedEventHandler
{
    private PrivateMessagePublisher $privateMessagePublisher;

    #[Required]
    public function setPrivateMessagePublisher(PrivateMessagePublisher $privateMessagePublisher): self
    {
        $this->privateMessagePublisher = $privateMessagePublisher;
        return $this;
    }

    public function __invoke(CreatedEvent $event)
    {
        $this->privateMessagePublisher->publishPrivateMessageCreatedEvent($event);
    }
}
