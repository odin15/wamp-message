<?php

namespace App\Amqp\Handler\EventHandler\Group;

use App\Amqp\Message\Event\Group\UpdatedEvent;
use App\WampServer\Publisher\GroupPublisher;
use Symfony\Contracts\Service\Attribute\Required;

class UpdatedEventHandler
{
    private GroupPublisher $groupPublisher;

    #[Required]
    public function setGroupPublisher(GroupPublisher $groupPublisher): self
    {
        $this->groupPublisher = $groupPublisher;
        return $this;
    }

    public function __invoke(UpdatedEvent $event)
    {
        $this->groupPublisher->publishGroupUpdatedEvent($event);
    }
}
