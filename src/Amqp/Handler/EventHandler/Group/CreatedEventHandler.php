<?php

namespace App\Amqp\Handler\EventHandler\Group;

use App\Amqp\Message\Event\Group\CreatedEvent;
use App\WampServer\Publisher\GroupPublisher;
use Symfony\Contracts\Service\Attribute\Required;

class CreatedEventHandler
{
    private GroupPublisher $groupPublisher;

    #[Required]
    public function setGroupPublisher(GroupPublisher $groupPublisher): self
    {
        $this->groupPublisher = $groupPublisher;
        return $this;
    }

    public function __invoke(CreatedEvent $event)
    {
        $this->groupPublisher->publishGroupCreatedEvent($event);
    }
}
