<?php

namespace App\Amqp\Handler\EventHandler\Group;

use App\Amqp\Message\Event\Group\DeletedEvent;
use App\WampServer\Publisher\GroupPublisher;
use Symfony\Contracts\Service\Attribute\Required;

class DeletedEventHandler
{
    private GroupPublisher $groupPublisher;

    #[Required]
    public function setGroupPublisher(GroupPublisher $groupPublisher): self
    {
        $this->groupPublisher = $groupPublisher;
        return $this;
    }

    public function __invoke(DeletedEvent $event)
    {
        $this->groupPublisher->publishGroupDeletedEvent($event);
    }
}
