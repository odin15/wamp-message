<?php

namespace App\Amqp\Handler\EventHandler\GroupUser;

use App\Amqp\Message\Event\GroupUser\CreatedEvent;
use App\WampServer\Publisher\GroupUserPublisher;
use Symfony\Contracts\Service\Attribute\Required;

class CreatedEventHandler
{
    private GroupUserPublisher $groupUserPublisher;

    #[Required]
    public function setGroupUserPublisher(GroupUserPublisher $groupUserPublisher): self
    {
        $this->groupUserPublisher = $groupUserPublisher;
        return $this;
    }

    public function __invoke(CreatedEvent $event)
    {
        $this->groupUserPublisher->publishGroupUserCreatedEvent($event);
    }
}
