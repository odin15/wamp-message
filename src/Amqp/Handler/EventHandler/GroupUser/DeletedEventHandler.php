<?php

namespace App\Amqp\Handler\EventHandler\GroupUser;

use App\Amqp\Message\Event\GroupUser\DeletedEvent;
use App\WampServer\Publisher\GroupUserPublisher;
use Symfony\Contracts\Service\Attribute\Required;

class DeletedEventHandler
{
    private GroupUserPublisher $groupUserPublisher;

    #[Required]
    public function setGroupUserPublisher(GroupUserPublisher $groupUserPublisher): self
    {
        $this->groupUserPublisher = $groupUserPublisher;
        return $this;
    }

    public function __invoke(DeletedEvent $event)
    {
        $this->groupUserPublisher->publishGroupUserDeletedEvent($event);
    }
}
