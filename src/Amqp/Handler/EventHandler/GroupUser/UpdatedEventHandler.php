<?php

namespace App\Amqp\Handler\EventHandler\GroupUser;

use App\Amqp\Message\Event\GroupUser\UpdatedEvent;
use App\WampServer\Publisher\GroupUserPublisher;
use Symfony\Contracts\Service\Attribute\Required;

class UpdatedEventHandler
{
    private GroupUserPublisher $groupUserPublisher;

    #[Required]
    public function setGroupUserPublisher(GroupUserPublisher $groupUserPublisher): self
    {
        $this->groupUserPublisher = $groupUserPublisher;
        return $this;
    }

    public function __invoke(UpdatedEvent $event)
    {
        $this->groupUserPublisher->publishGroupUserUpdatedEvent($event);
    }
}
