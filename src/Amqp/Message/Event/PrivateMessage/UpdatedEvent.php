<?php

namespace App\Amqp\Message\Event\PrivateMessage;

use App\Model\PrivateMessage;

class UpdatedEvent
{
    private PrivateMessage $privateMessage;

    public function getPrivateMessage(): PrivateMessage
    {
        return $this->privateMessage;
    }

    public function setPrivateMessage(PrivateMessage $privateMessage): self
    {
        $this->privateMessage = $privateMessage;
        return $this;
    }
}
