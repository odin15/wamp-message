<?php

namespace App\Amqp\Message\Event\GroupUser;

use App\Model\GroupUser;

class DeletedEvent
{
    private GroupUser $groupUser;

    public function getGroupUser(): GroupUser
    {
        return $this->groupUser;
    }

    public function setGroupUser(GroupUser $groupUser): self
    {
        $this->groupUser = $groupUser;
        return $this;
    }
}
