<?php

namespace App\Amqp\Message\Event\Group;

use App\Model\Group;

class CreatedEvent
{
    private Group $group;

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function setGroup(Group $group): self
    {
        $this->group = $group;
        return $this;
    }
}
