<?php

namespace App\Amqp\Message\Event\Group;

use App\Model\Group;

class UpdatedEvent
{
    private Group $group;

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function setGroup(Group $group): self
    {
        $this->group = $group;
        return $this;
    }
}
