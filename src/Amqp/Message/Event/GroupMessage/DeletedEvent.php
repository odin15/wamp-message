<?php

namespace App\Amqp\Message\Event\GroupMessage;

use App\Model\GroupMessage;

class DeletedEvent
{
    private GroupMessage $groupMessage;

    public function getGroup(): GroupMessage
    {
        return $this->groupMessage;
    }

    public function setGroup(GroupMessage $groupMessage): self
    {
        $this->groupMessage = $groupMessage;
        return $this;
    }
}
