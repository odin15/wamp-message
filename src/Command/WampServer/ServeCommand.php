<?php

namespace App\Command\WampServer;

use App\WampServer\WampRouter;
use React\EventLoop\Loop;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:wamp-server:start', description: 'Start a Wamp Server')]
class ServeCommand extends Command
{
    public function __construct(private WampRouter $wampRouter)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Starting Wamp Server');

        $this->wampRouter->start(false);
        Loop::get()->run();

        return 0;
    }
}
