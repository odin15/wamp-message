<?php

namespace App\WampClient;

use App\Log\LoggerAwareTrait;
use Thruway\AbstractSession;
use Thruway\Message\Message;
use Thruway\Role\AbstractRole;

class WampClientLoggerRole extends AbstractRole
{
    use LoggerAwareTrait;

    public function onMessage(AbstractSession $session, Message $msg): mixed
    {
        $this->logger->info('New request received');

        return null;
    }

    public function handlesMessage(Message $msg): bool
    {
        return true;
    }
}
