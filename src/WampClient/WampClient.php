<?php

namespace App\WampClient;

use App\Log\LoggerAwareTrait;
use React\Promise\PromiseInterface;
use Thruway\Peer\Client;

class WampClient extends Client
{
    use LoggerAwareTrait;

    public function register(string $topicName, $callable): void
    {
        $this->session->register($topicName, function ($args) use ($callable) {
            try {
                $reflectionMethod = new \ReflectionMethod($callable[0], $callable[1]);
                $parameters = $reflectionMethod->getParameters();
                if (count($parameters) !== count($args)) {
                    throw new \RuntimeException('Invalid number of arguments');
                }

                call_user_func_array($callable, $args);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        });
    }

    public function subscribe(string $topicName, $callable): void
    {
        $this->session->subscribe($topicName, function ($args, $kwargs) use ($callable) {
            try {
                $callable($args, $kwargs);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        });
    }

    public function publish($topicName, $arguments = [], $argumentsKw = []): PromiseInterface
    {
        return $this->getSession()->publish(
            $topicName,
            $arguments,
            $argumentsKw,
            ['acknowledge' => true]
        )->then(function ($value) {
            return $value;
        }, function ($error) {
            return $error;
        });
    }
}
