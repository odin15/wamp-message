<?php

namespace App;

use App\DependencyInjection\Compiler\AuthorizationManagerPass;
use App\DependencyInjection\Compiler\AuthProviderClientPass;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new AuthProviderClientPass());
        $container->addCompilerPass(new AuthorizationManagerPass());
    }
}
