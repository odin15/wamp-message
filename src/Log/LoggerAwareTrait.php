<?php

namespace App\Log;

use Psr\Log\LoggerInterface;

trait LoggerAwareTrait
{
    /** @var LoggerInterface */
    protected $logger;

    /** @required */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
