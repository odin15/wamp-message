<?php

namespace App\DependencyInjection\Compiler;

use App\WampServer\WampRouter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AuthProviderClientPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        /** @var bool $exists */
        $exists = $container->has(WampRouter::class);
        if (!$exists) {
            return;
        }

        $definition = $container->findDefinition(WampRouter::class);

        $taggedServices = $container->findTaggedServiceIds('app.wamp_server.security.auth_provider_client');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addAuthProviderClient', [new Reference($id)]);
        }
    }
}
