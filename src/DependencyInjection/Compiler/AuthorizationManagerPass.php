<?php

namespace App\DependencyInjection\Compiler;

use App\WampServer\WampRouter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AuthorizationManagerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        /** @var bool $exists */
        $exists = $container->has(WampRouter::class);
        if (!$exists) {
            return;
        }

        $definition = $container->findDefinition(WampRouter::class);

        $taggedServices = $container->findTaggedServiceIds('app.wamp_server.security.authorization_manager');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addAuthorizationManager', [new Reference($id)]);
        }
    }
}
