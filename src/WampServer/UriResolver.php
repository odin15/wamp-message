<?php

namespace App\WampServer;

class UriResolver
{
    public function resolve(string $topic, array $parameters = []): string
    {
        foreach ($parameters as $name => $value) {
            $topic = str_replace('{'.$name.'}', $value, $topic);
        }

        return $topic;
    }
}
