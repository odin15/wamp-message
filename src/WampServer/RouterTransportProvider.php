<?php

namespace App\WampServer;

use Thruway\Transport\RatchetTransportProvider;

class RouterTransportProvider extends RatchetTransportProvider
{
    public function __construct(private string $address, private int|string $port)
    {
        parent::__construct($address, $port);
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPort(): int|string
    {
        return $this->port;
    }

    public function setPort(int|string $port): self
    {
        $this->port = $port;

        return $this;
    }
}
