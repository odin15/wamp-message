<?php

namespace App\WampServer;

use React\Promise\Deferred;

class Message
{
    public function __construct(
        private string $realm,
        private string $topic,
        private array $payload,
        private Deferred $deferred
    ) {
    }

    public function getRealm(): ?string
    {
        return $this->realm;
    }

    public function setRealm(string $realm): self
    {
        $this->realm = $realm;

        return $this;
    }

    public function getTopic(): ?string
    {
        return $this->topic;
    }

    public function setTopic(string $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    public function getPayload(): ?array
    {
        return $this->payload;
    }

    public function setPayload(array $payload): self
    {
        $this->payload = $payload;

        return $this;
    }

    public function getDeferred(): ?Deferred
    {
        return $this->deferred;
    }

    public function setDeferred(Deferred $deferred): self
    {
        $this->deferred = $deferred;

        return $this;
    }
}
