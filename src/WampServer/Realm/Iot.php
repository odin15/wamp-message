<?php

namespace App\WampServer\Realm;

class Iot
{
    public const REALM_NAME = 'iot';

    public const TOPIC_PRIVATE_MESSAGE_CREATED = 'private.message.created.{userId}';
    public const TOPIC_PRIVATE_MESSAGE_UPDATED = 'private.message.updated.{userId}';
    public const TOPIC_PRIVATE_MESSAGE_DELETED = 'private.message.deleted.{userId}';

    public const TOPIC_GROUP_CREATED = 'group.created.{userId}';
    public const TOPIC_GROUP_UPDATED = 'group.updated.{groupId}';
    public const TOPIC_GROUP_DELETED = 'group.deleted.{groupId}';

    public const TOPIC_GROUP_MESSAGE_CREATED = 'group.message.created.{groupId}';
    public const TOPIC_GROUP_MESSAGE_UPDATED = 'group.message.updated.{groupId}';
    public const TOPIC_GROUP_MESSAGE_DELETED = 'group.message.deleted.{groupId}';
}
