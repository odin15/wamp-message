<?php

namespace App\WampServer;

use App\WampServer\Security\AuthorizationManager\AbstractAuthorizationManager;
use React\EventLoop\Loop;
use Thruway\Authentication\AbstractAuthProviderClient;
use Thruway\Authentication\AuthenticationManager;
use Thruway\Peer\Router;

class WampRouter extends Router
{
    public function __construct(RouterTransportProvider $routerTransportProvider)
    {
        parent::__construct(Loop::get());

        $this->addTransportProvider($routerTransportProvider);
        $this->registerModule(new AuthenticationManager());
    }

    public function addAuthProviderClient(AbstractAuthProviderClient $authProviderClient): void
    {
        $this->addInternalClient($authProviderClient);
    }

    public function addAuthorizationManager(AbstractAuthorizationManager $authorizationManager): void
    {
        $this->registerModule($authorizationManager);
    }
}
