<?php

namespace App\WampServer\Security\AuthorizationManager;

use App\WampServer\Realm\Web;

class WebAuthorizationManager extends AbstractAuthorizationManager
{
    public function getRealmName(): string
    {
        return Web::REALM_NAME;
    }
}
