<?php

namespace App\WampServer\Security\AuthorizationManager;

use App\WampServer\Security\AuthorizationRules;
use React\EventLoop\Loop;
use Thruway\Authentication\AuthenticationDetails;
use Thruway\Authentication\AuthorizationManager as BaseAuthorizationManager;
use Thruway\Message\ActionMessageInterface;
use Thruway\Session;

abstract class AbstractAuthorizationManager extends BaseAuthorizationManager
{
    abstract public function getRealmName(): string;

    public function __construct(protected AuthorizationRules $authorizationRules)
    {
        /* @phpstan-ignore-next-line */
        parent::__construct($this->getRealmName(), Loop::get());
    }

    public function isAuthorizedTo(Session $session, ActionMessageInterface $actionMsg): bool
    {
        $action = $actionMsg->getActionName();
        $uri = $actionMsg->getUri();

        $authenticationDetails = $session->getAuthenticationDetails();

        // admin can do anything - pretty important
        // if this isn't here - then we can't set up any other rules
        if ($authenticationDetails->hasAuthRole('admin')) {
            return true;
        }

        if (!$this->isReady()) {
            return false;
        }

        $authorizations = $this->authorizationRules->getAuthorizations($this->getRealmName());
        if (null === $authorizations || !isset($authorizations[$action])) {
            return false;
        }

        $map = $this->getMap($authenticationDetails);

        foreach ($authorizations[$action] as $role => $authorizedUris) {
            if (!$authenticationDetails->hasAuthRole($role)) {
                continue;
            }

            foreach ($authorizedUris as $authorizedUri => $parameters) {
                if (is_array($parameters)) {
                    $parameters = $this->fillParameters($parameters, $map);

                    $pattern = $this->uriToRegex($authorizedUri, $parameters);
                    if (preg_match($pattern, $uri)) {
                        return true;
                    }
                } else {
                    if ($authorizedUri === $uri) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected function fillParameters(array $parameters, array $map): array
    {
        foreach ($parameters as $key => $value) {
            $parameters[$key] = str_replace(
                array_keys($map),
                array_values($map),
                $value
            );
        }

        return $parameters;
    }

    protected function getMap(AuthenticationDetails $authenticationDetails): array
    {
        return [
            'authId' => $authenticationDetails->getAuthId(),
            'any' => '([^.]+)',
        ];
    }

    protected function uriToRegex(string $authorizedUri, array $parameters = []): string
    {
        $authorizedUri = str_replace([
            '.',
        ], [
            '\\.',
        ], $authorizedUri);

        foreach ($parameters as $name => $value) {
            $authorizedUri = str_replace('{'.$name.'}', $value, $authorizedUri);
        }

        return '/^'.$authorizedUri.'$/';
    }
}
