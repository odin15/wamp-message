<?php

namespace App\WampServer\Security\AuthorizationManager;

use App\WampServer\Realm\Iot;

class IotAuthorizationManager extends AbstractAuthorizationManager
{
    public function getRealmName(): string
    {
        return Iot::REALM_NAME;
    }
}
