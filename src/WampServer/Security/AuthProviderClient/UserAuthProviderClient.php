<?php

namespace App\WampServer\Security\AuthProviderClient;

use App\WampServer\Realm\Web;
use App\WampServer\WampRole;
use Odevia\MicroserviceClientBundle\ApiGateway\ApiGateway;
use React\EventLoop\Loop;

class UserAuthProviderClient extends AbstractWampCraAuthProviderClient
{
    public const METHOD_NAME = 'user_wampcra';

    public function __construct(private ApiGateway $apiGateway)
    {
        parent::__construct([Web::REALM_NAME], Loop::get());
    }

    public function getMethodName(): string
    {
        return self::METHOD_NAME;
    }

    protected function getKeyFromAuthId(string $authId): ?string
    {
        $query = '
            query getToken($userId: ID!) {
              getToken(userId: $userId, type: "TYPE_AUTHENTICATION") {
                value
              }
            }
        ';

        $response = $this->apiGateway->graphQLQuery('api-gateway', $query, ['userId' => $authId]);
        $data = $response->getData();
        if (!isset($data['getToken'])) {
            return null;
        }

        return $data['getToken']['value'];
    }

    protected function getRoleFromAuthId(string $authId): ?string
    {
        return WampRole::ROLE_USER;
    }
}
