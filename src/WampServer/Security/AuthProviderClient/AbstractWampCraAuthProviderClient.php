<?php

namespace App\WampServer\Security\AuthProviderClient;

use App\WampServer\Security\Cors;
use Symfony\Contracts\Service\Attribute\Required;
use Thruway\Authentication\AbstractAuthProviderClient;
use Thruway\Message\HelloMessage;
use Thruway\Message\Message;

abstract class AbstractWampCraAuthProviderClient extends AbstractAuthProviderClient
{
    protected Cors $cors;

    abstract protected function getKeyFromAuthId(string $authId): ?string;

    abstract protected function getRoleFromAuthId(string $authId): ?string;

    #[Required]
    public function setCors(Cors $cors)
    {
        $this->cors = $cors;
    }

    public function getMethodName(): string
    {
        return 'wampcra';
    }

    protected function getOriginCheck(): bool
    {
        return true;
    }

    public function processHello(array $args): array
    {
        if ($this->getOriginCheck()) {
            $origin = $args[0][2]->transport->headers->Origin[0];
            if (!$this->cors->isOriginAllowed($origin)) {
                return ['ERROR'];
            }
        }

        $helloMsg = array_shift($args);
        $sessionInfo = array_shift($args);

        if (!is_array($helloMsg) || !is_object($sessionInfo)) {
            return ['ERROR'];
        }

        $helloMsg = Message::createMessageFromArray($helloMsg);

        if (!$helloMsg instanceof HelloMessage
            || !is_object($sessionInfo)
            || !isset($sessionInfo->sessionId)
            || !isset($helloMsg->getDetails()->authid)
        ) {
            return ['ERROR'];
        }

        $authid = $helloMsg->getDetails()->authid;

        $challenge = [
            'authid' => $authid,
            'authmethod' => $this->getMethodName(),
            'nonce' => bin2hex(openssl_random_pseudo_bytes(22)),
            'timestamp' => date('Y-m-d\TH:i:sO'),
            'session' => $sessionInfo->sessionId,
        ];

        $serializedChallenge = json_encode($challenge);
        $challengeDetails = [
            'challenge' => $serializedChallenge,
            'challenge_method' => $this->getMethodName(),
        ];

        return ['CHALLENGE', $challengeDetails];
    }

    /**
     * Process authenticate.
     *
     * @param mixed $signature
     * @param mixed $extra
     */
    public function processAuthenticate($signature, $extra = null): array
    {
        $challenge = $this->getChallengeFromExtra($extra);

        if (!$challenge
            || !isset($challenge->authid)
        ) {
            return ['FAILURE'];
        }

        $authid = $challenge->authid;

        $key = $this->getKeyFromAuthId($authid);

        if (null === $key) {
            return ['FAILURE'];
        }

        $hash = base64_encode(hash_hmac('sha256', json_encode($challenge), $key, true));

        if ($hash !== $signature) {
            return ['FAILURE'];
        }

        $authDetails = [
            'authmethod' => $this->getMethodName(),
            'authrole' => $this->getRoleFromAuthId($authid),
            'authid' => $challenge->authid,
        ];

        return ['SUCCESS', $authDetails];
    }

    /**
     * Gets the Challenge Message from the extra object.
     *
     * @param object $extra
     *
     * @return bool|\stdClass
     */
    protected function getChallengeFromExtra($extra)
    {
        return (is_object($extra)
            && isset($extra->challenge_details)
            && is_object($extra->challenge_details)
            && isset($extra->challenge_details->challenge))
            ? json_decode($extra->challenge_details->challenge)
            : false
        ;
    }
}
