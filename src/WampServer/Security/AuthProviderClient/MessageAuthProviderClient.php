<?php

namespace App\WampServer\Security\AuthProviderClient;

use App\WampServer\WampRole;
use React\EventLoop\Loop;

class MessageAuthProviderClient extends AbstractWampCraAuthProviderClient
{
    public function __construct(private string $authToken)
    {
        parent::__construct(['*'], Loop::get());
    }

    public function getMethodName(): string
    {
        return 'message_wampcra';
    }

    protected function getKeyFromAuthId(string $authId): ?string
    {
        return $this->authToken;
    }

    protected function getRoleFromAuthId(string $authId): ?string
    {
        return WampRole::ROLE_SERVICE_INTERNAL;
    }
}

