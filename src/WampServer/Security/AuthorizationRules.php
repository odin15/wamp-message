<?php

namespace App\WampServer\Security;

class AuthorizationRules
{
    private array $authorizations;

    public function __construct(array $authorizations)
    {
        $this->authorizations = $authorizations;
    }

    public function getAuthorizations(string $realm): ?array
    {
        if (!isset($this->authorizations[$realm])) {
            return null;
        }

        return $this->authorizations[$realm];
    }
}
