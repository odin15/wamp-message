<?php

namespace App\WampServer\Security;

class Cors
{
    public function __construct(private ?string $crossOriginDomainRestriction = null)
    {
    }

    public function isOriginAllowed($origin): bool
    {
        if ($this->crossOriginDomainRestriction) {
            return preg_match('/^(http)s?:\/\/(www\.)?('.$this->crossOriginDomainRestriction.')(:?\/.*)?$/', $origin);
        }

        return true;
    }
}
