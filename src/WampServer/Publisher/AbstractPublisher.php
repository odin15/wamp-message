<?php

namespace App\WampServer\Publisher;

use React\Promise\PromiseInterface;
use Symfony\Contracts\Service\Attribute\Required;

class AbstractPublisher
{
    private Publisher $publisher;

    #[Required]
    public function setPublisher(Publisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public function publish($realms, string $topic, array $payload = [], array $parameters = []): PromiseInterface
    {
        return $this->publisher->publish($realms, $topic, $payload, $parameters);
    }
}
