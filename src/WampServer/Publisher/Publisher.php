<?php

namespace App\WampServer\Publisher;

use App\WampClient\WampClient;
use App\WampServer\Message;
use App\WampServer\Security\ClientAuthenticator\ServiceClientAuthenticator;
use App\WampServer\UriResolver;
use React\EventLoop\Loop;
use function React\Promise\all;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Thruway\ClientSession;
use Thruway\Transport\PawlTransportProvider;

class Publisher
{
    public const STATUS_CLOSED = 'closed';
    public const STATUS_PAUSED = 'paused';
    public const STATUS_OPEN = 'open';

    /** @var WampClient[] */
    private array $wampClients = [];
    /** @var Message[][] */
    private array $messages = [];
    /** @var string[] */
    private array $statuses = [];

    private string $host;
    private string $port;

    private UriResolver $uriResolver;
    private ServiceClientAuthenticator $serviceClientAuthenticator;
    private NormalizerInterface $normalizer;

    public function __construct(
        string $host,
        string $port,
        UriResolver $uriResolver,
        ServiceClientAuthenticator $serviceClientAuthenticator,
        NormalizerInterface $normalizer
    ) {
        $this->host = $host;
        $this->port = $port;
        $this->uriResolver = $uriResolver;
        $this->serviceClientAuthenticator = $serviceClientAuthenticator;
        $this->normalizer = $normalizer;
    }

    public function publish(string|array $realms, string $topic, array $payload = [], array $parameters = []): PromiseInterface
    {
        $topic = $this->uriResolver->resolve($topic, $parameters);

        if (count($payload)) {
            $payload = $this->normalizer->normalize($payload);
        }

        if (!is_array($realms)) {
            $realms = [$realms];
        }

        $loop = Loop::get();
        $promises = [];
        foreach ($realms as $realm) {
            $promises[] = $this->addMessage($realm, $topic, $payload)->then(function () use ($loop) {
                $hasMessage = false;
                foreach ($this->messages as $messages) {
                    if (count($messages)) {
                        $hasMessage = true;
                        break;
                    }
                }

                if (!$hasMessage) {
                    $loop->stop();
                }
            });
        }

        $loop->run();

        gc_collect_cycles();

        return all($promises);
    }

    private function getWampClient(string $realm): WampClient
    {
        if (!isset($this->wampClients[$realm])) {
            $this->createClient($realm);
        }

        return $this->wampClients[$realm];
    }

    private function createClient(string $realm): void
    {
        $wampClient = new WampClient($realm, Loop::get());

        $transportProvider = new PawlTransportProvider('ws://'.$this->host.':'.$this->port.'/');
        $wampClient->addTransportProvider($transportProvider);
        $wampClient->addClientAuthenticator($this->serviceClientAuthenticator);

        $this->wampClients[$realm] = $wampClient;
        $this->statuses[$realm] = self::STATUS_CLOSED;

        $wampClient->on('open', function (ClientSession $session) use ($realm) {
            $this->statuses[$realm] = self::STATUS_OPEN;
            $this->flushMessages($realm);
        });
        $wampClient->on('close', function ($session) use ($realm) {
            $this->statuses[$realm] = self::STATUS_CLOSED;
        });

        $wampClient->start(false);
    }

    private function addMessage(string $realm, $topic, $payload): PromiseInterface
    {
        if (!isset($this->statuses[$realm]) || self::STATUS_OPEN !== $this->statuses[$realm]) {
            $id = $this->generateUniqueId();
            $deferred = new Deferred();

            if (!isset($this->messages[$realm])) {
                $this->messages[$realm] = [];
            }
            $this->messages[$realm][$id] = new Message($realm, $topic, $payload, $deferred);

            // Force to instanciate the WampClient if it doesn't exist yet
            $this->getWampClient($realm);

            return $deferred->promise();
        } else {
            return $this->performRealPublish($realm, $topic, $payload);
        }
    }

    private function flushMessages(string $realm): void
    {
        foreach ($this->messages[$realm] as $id => $message) {
            $this->performRealPublish(
                $message->getRealm(),
                $message->getTopic(),
                $message->getPayload()
            )->then(function ($value) use ($realm, $message, $id) {
                unset($this->messages[$realm][$id]);
                $message->getDeferred()->resolve($value);
            });
        }
    }

    private function performRealPublish(string $realm, string $topic, array $payload = []): PromiseInterface
    {
        $wampClient = $this->getWampClient($realm);

        return $wampClient->publish(
            $topic,
            [],
            $payload
        );
    }

    private function generateUniqueId(): string
    {
        return uniqid('request');
    }
}