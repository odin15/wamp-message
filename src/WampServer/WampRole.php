<?php

namespace App\WampServer;

class WampRole
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_SERVICE_INTERNAL = 'ROLE_SERVICE_INTERNAL';
}
