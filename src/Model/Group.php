<?php

namespace App\Model;

class Group
{
    private int $id;

    private string $name;

    private array $groupUsersIds;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getGroupUsersIds(): array
    {
        return $this->groupUsersIds;
    }

    public function setGroupUsersIds(array $groupUsersIds): self
    {
        $this->groupUsersIds = $groupUsersIds;
        return $this;
    }
}
