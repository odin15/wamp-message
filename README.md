Wamp Server Service
============

This service is responsible for serving a Web Socket Server based on [WAMP](https://wamp-proto.org/) :
* Allow users and devices to connect
* Converts AMQP messages through Web Socket
* ...

## Realms

This service provides two realms :
* `web` : used to publish event to web applications (users)
* `iot` : used to communicate with connected devices (station-watcher, traffic-box, etc...)

Installation
============

### Option 1 (recommanded) : Docker Compose

See full configuration on [GitLab](https://gitlab.dyosis.fr/odin/odin/-/tree/master)

### Option 2 : Docker

_This service require an AMQP server, and an Odin's API-Gateway service to work._

Here is a working example using 2 networks :
* odin : enable Updated micro-services to communicate through HTTP
* amqp : enable this service to communicate with AMQP server

```console
docker create \
  --name wamp-server.odin \
  --network=odevia \
  -e APP_ENV='prod' \
  -e WEB_SOCKET_ADDRESS='0.0.0.0' \
  -e WAMP_SERVER_HOST='127.0.0.1' \
  -e WAMP_SERVER_PORT='9000' \
  -e CROSS_ORIGIN_DOMAIN_RESTRICTION='0' \
  -e STATION_WATCHER_ACCESS_TOKEN='ThisTokenIsNotSecretChangeIt' \
  -e TRAFFIC_BOX_ACCESS_TOKEN='ThisTokenIsNotSecretChangeIt' \
  -e STATIONS_ACCESS_TOKEN='ThisTokenIsNotSecretChangeIt' \
  -e MICROSERVICE_ACCESS_TOKEN='ThisTokenIsNotSecretChangeIt' \
  -e API_GATEWAY_BASE_URL='http://api-gateway.odevia' \
  -e AMQP_USER='guest' \
  -e AMQP_PASSWORD='guest' \
  -e AMQP_HOST='amqp' \
  -e AMQP_PORT='5672' \
  -e AMQP_VIRTUAL_HOST='odin' \
  registry.gitlab.dyosis.fr/odevia/wamp-server:master \
&& docker network connect amqp wamp-server.odevia \
&& docker start wamp-server.odevia
```

Configuration
=============

### Environment variables

* `WEB_SOCKET_ADDRESS` : IP address to listen (0.0.0.0 equals "from everywhere")
* `WAMP_SERVER_HOST` : IP adress of the Wamp Router the Client should connect to
* `WAMP_SERVER_PORT` : Port used by the Web Socket Server
* `CROSS_ORIGIN_DOMAIN_RESTRICTION` : Regex to restrict domains able to connect
* `STATION_WATCHER_ACCESS_TOKEN` : Token to authenticate as a StationWatcher on WAMP
* `TRAFFIC_BOX_ACCESS_TOKEN` : Token to authenticate as a TrafficBox on WAMP
* `STATIONS_ACCESS_TOKEN` : Token to authenticate as "stations" service on WAMP
* `MICROSERVICE_ACCESS_TOKEN` : Token to authenticate as a service on Odevia
* `API_GATEWAY_BASE_URL` : URL to access Odevia API-Gateway (doesn't need public access if the api-gateway service is on the same network)

### Networking

The `Wamp Server` service needs to access other services :
* AMQP server
* Odin's API-Gateway service

_This service doesn't need to be exposed publicly (eg. Traefik), as it's accessed by other services through Odin's API-Gateway service_
