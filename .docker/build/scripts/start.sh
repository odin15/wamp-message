#!/bin/bash

# Wait until API Gateway is ready
echo 'Waiting for API Gateway...'
./.docker/build/scripts/wait-for-http.sh ${API_GATEWAY_BASE_URL}/alive

# Wait until AMQP Server is ready
echo 'Waiting for AMQP...'
./.docker/build/scripts/wait-for-it.sh ${AMQP_HOST}:${AMQP_PORT} --timeout=120

/usr/bin/supervisord -c /var/www/.docker/build/conf/supervisord/supervisord.conf
