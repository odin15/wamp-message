FROM php:8.1

WORKDIR /var/www/

ARG GITLAB_ACCESS_TOKEN
ENV SERVER_NAME=localhost

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

RUN     apt-get update \
    &&  apt-get install -y --no-install-recommends \
			locales \
			apt-utils \
			git \
			libmcrypt-dev \
			libcurl3-dev \
            librabbitmq-dev \
            libssh-dev \
			gnupg2 \
			libzip-dev \
			unzip \
    &&  pecl config-set php_ini "${PHP_INI_DIR}/php.ini" \
    &&  docker-php-ext-install -j$(nproc) \
            bcmath \
            sockets \
			curl \
			opcache \
			zip \
    &&  pecl install \
            amqp \
	&&  docker-php-ext-enable \
            amqp

# Project files
COPY --chown=www-data:www-data ./ ./

# Install Composer
RUN 	curl -sSk https://getcomposer.org/installer | php -- --disable-tls \
    &&	mv composer.phar /usr/local/bin/composer \
    && 	composer config --global --auth gitlab-oauth.gitlab.odeven.fr $GITLAB_ACCESS_TOKEN \
    && 	composer install
